﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Collections;
using Cellario.CellController;
using System.Threading.Tasks;
using Cellario.DeviceBase;

namespace Cellario.Hooks
{
    public class BeamBarcodeHook : ICell
    {
        private Scheduler _cell;

        public Scheduler Cell
        {
            get { return _cell; }
            set
            {
                // Set the scheduler value for quick access
                _cell = value;

                // If the value is null, we do not want to subscribe
                if (_cell == null)
                    return;

                _cell.NewOrderStarted -= HandleNewOrderStarted;
                _cell.NewOrderStarted += HandleNewOrderStarted;
            }
        }

        private void HandleNewOrderStarted(RunOrder runOrder)
        {
            // Subscribe to current order SampleOperationChange event.
            // This event fires whenever any of the samples (labware) 
            // starts, finish or fail any operation (Move, Dispense, LiquidTransfer, etc.).
            runOrder.SampleOperationChange -= HandleSampleOperationChange;
            runOrder.SampleOperationChange += HandleSampleOperationChange;
            _cell.AddMessage(NotificationLevel.Info, "I am running in the right thread");
        }

        private void HandleSampleOperationChange(SampleOperation sender)
        {
            //modify the trackerPath to indicate the path to the tracker file. 
            //The format should be trackerPath = @"YOUR_FILE_PATH";
            const string _trackerPath = @"C:\Users\ayuzvik\Desktop\MC958\Tracker\Tracker2.csv";
            const string _trackerPlateinsequence = "PlateInSequence";
            const string _trackerNumberOfPlates = "NumberOfPlates";
            var writeReturn = String.Empty;
            var positionNum = 0;
            var hookThreadName = "Barcode Plates";
            int defCol = 1;
            int defRow = 1;
            int posCol = 0;
            int posRow = 0;
            int totalInStack = 24;
            int totalStackerCount = 14;
            int numberOfBarcodedPlates = 20;

            var threadname = sender.sample.InputSample.runOrder.PlateProtocols.SingleOrDefault(n => n.Value.Name == hookThreadName).Value?.Name;
            
            //if there is no thread called "Barcode Plates" do not run the hook.
            if (string.IsNullOrEmpty(threadname)) return;

            //Store current operation
            var curOperation = sender.OperationName;


            if (threadname != hookThreadName || 
                curOperation != enumOperation.OpenDoor.ToString() || 
                sender.Notes != "STARTED" || 
                sender.sample.InputSample.CompletedSteps.Count != 0)
                return;

            //read file and store values for number in sequence and total number of barcoded plates
            //writeReturn = File.ReadAllText(_trackerPath);

            writeReturn = ReadTrackerFile(_trackerPath, _trackerPlateinsequence);
            numberOfBarcodedPlates = int.Parse(ReadTrackerFile(_trackerPath, _trackerNumberOfPlates));
            //TODO check for more then one line or more values then expected


            //convert string to int value
            positionNum = int.Parse(writeReturn);

            //Column & Row calculation logic
            //if value in the tracker file exceeds total number of position in stacker
            //increment the column and use % to determine the number of plates that would need to 
            //overflow in the next stacker
            if (positionNum > totalInStack)
            {
                //check if the plates were stored in the last stacker
                //this "if" handles the case of overflowing the last stacker
                // with plates that end up in the first stacker and on post math.
                //todo add different cases for liconic 10 and ambi 14
                if (defCol == totalStackerCount)
                {
                    posCol = 1 + (positionNum / totalInStack);
                    posRow = positionNum % totalInStack;
                }
                //else do the regular calculation with regular stacker overflow
                else
                {
                    posCol = defCol + (positionNum / totalInStack);
                    posRow = positionNum % totalInStack;
                }

            }
            //else if the number of barcoded plates is not higher then the number of rows in a stacker
            //set the column equal to default stacker (whatever stacker the plate came from)
            //and set the row to be equal to teh value from teh tracker file.
            else
            {
                posCol = defCol;
                posRow = positionNum;
            }

            //Tracker file iteration logic
            //if tracker reaches max value 20, reset that value to 1 instead of incrementing it
            //else increment the value from the tracker file by 1
            //Debug messages for convenience.
            if (positionNum >= numberOfBarcodedPlates)
            {
                _cell.AddMessage(NotificationLevel.Info, "At the last plate, restarting a sequence");
                positionNum = 1;
            }
            else
            {
                _cell.AddMessage(NotificationLevel.Info, "+1 to sequence");
                positionNum++;
            }

            sender.sample.InputSample.StartingPosition.Column = posCol;
            sender.sample.InputSample.StartingPosition.Row = posRow;
            sender.sample.InputSample.StartingPosition.UpdateDb();

            //Uncomment for Debugging
            //_cell.AddMessage(NotificationLevel.Info,
            //    "Col POST LOGIC is: " + sender.sample.InputSample.StartingPosition.Column);
            //_cell.AddMessage(NotificationLevel.Info,
            //    "Row POST LOGIC is: " + sender.sample.InputSample.StartingPosition.Row);

            //turn int into string to write it to the tracker file
            writeReturn = positionNum.ToString();
            //overwrite value in the document
            WriteTrackerFile(_trackerPath, writeReturn, numberOfBarcodedPlates);
        }

        //helper function that reads the tracker file and returns the value assosiate with key
        public static string ReadTrackerFile(string trackerFilePath, string settingName)
        {
            var settingValue = string.Empty;
            var settingsFile = new StreamReader(trackerFilePath);
            while (!settingsFile.EndOfStream)
            {
                var line = settingsFile.ReadLine();
                var values = line.Split(',');
                var tempName = values[0];
                if (tempName != settingName) continue;
                settingValue = values[1];
                break;
            }
            settingsFile.Close();
            if (settingValue == String.Empty)
            {
                throw new Exception("Tracker file value is empty. Please input values in the tracker file before starting the run");
            }

            return settingValue;
        }

        //helper function that writes to the tracker file
        public static void WriteTrackerFile(string trackerFilePath, string writeValue, int stackSize)
        {
            var plateInSeqString = "PlateInSequence," + writeValue;
            var stackValue = "NumberOfPlates," + stackSize;
            var settingsFile = new StreamWriter(trackerFilePath);
            settingsFile.WriteLine(plateInSeqString + Environment.NewLine + stackValue);
            settingsFile.Close();
        }
    }
}